" contents of minimal .vimrc
execute pathogen#infect()
syntax on
filetype plugin indent on

let g:tern_show_argument_hints='on_hold'

set laststatus=2

colorscheme desert
set number
map <C-n> :NERDTreeToggle<CR>

noremap <C-J>     <C-W>j
noremap <C-K>     <C-W>k
noremap <C-H>     <C-W>h
noremap <C-L>     <C-W>l

